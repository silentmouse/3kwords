import Vue from 'vue'
import Vuex from 'vuex'
import words from './modules/words'
import progress from './modules/progress'
import user from './modules/user'
import session from './modules/session'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        words,
        progress,
        user,
        session
    }
})

export default store