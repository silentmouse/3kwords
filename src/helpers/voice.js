export function speakIt(text){
    speechSynthesis.speak(
        new SpeechSynthesisUtterance(text)
    );
}