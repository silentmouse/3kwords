#!/usr/bin/env bash

echo 'BEGIN COMPILE'
API_HOST=http://graphql.kwords.xyz/graphql npm run build
echo 'COMPILE'
git add -A
git commit -m "fix"
git push origin master
echo 'PUSHED'

WORK_DIR=$(mktemp -d)

function cleanup {
  rm -rf "$WORK_DIR"
  echo "Deleted temp working directory $WORK_DIR"
}

trap cleanup EXIT

NAMESPACE=dev
COMMIT=$(git rev-parse HEAD)
git archive HEAD | tar -x -C ${WORK_DIR}

cd ${WORK_DIR}

NAME=silentmouse/kwords-client
VER="${COMMIT::6}"
TAG="$NAME:latest"

docker build -t ${TAG} .
docker push ${TAG}

