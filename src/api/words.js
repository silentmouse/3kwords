export function get(limit, offset, mutation, func) {
    var query = `query Words($limit: Int!, $offset: Int) {
        words(offset: $offset, limit: $limit){id title translation transcription level audio}
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {offset, limit},
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('data returned:', payload.data)
            func(mutation, payload.data.words)
        });
}

export function getWordsFromList(params, mutation, func) {

    var user_id = params.user_id

    console.log("PRAMS", params)

    var query = `query getWordsFromList($user_id: String) {
        getWordsFromList(user_id: $user_id){id title translation transcription level audio}
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {user_id},
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('data returned:', payload.data)
            func(mutation, payload.data.getWordsFromList)
        });
}

export function getOne(id, mutation, func) {
    var query = `query Word($id: String!) {
        word(id: $id){id title translation transcription level audio
        meanings {
      noun {
        title
        definition
        synonyms {
          id
          title
        }
      }
      adjective {
        title
        definition
        synonyms {
          id
          title
        }
      }
      verb {
        title
        definition
        synonyms {
          id
          title
        }
      }
      adverb {
        title
        definition
        synonyms {
          id
          title
        }
      }
    }
    examples{
      noun {
        text
        definition
      }
      verb {
        text
        definition
      }
      adjective {
        text
        definition
      }
      adverb {
        text
        definition
      }
    }
    }
    }`;

    console.log("getONE", id)

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {id},
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('word data returned:', payload)
            func(mutation, payload.data.word)
        });
}