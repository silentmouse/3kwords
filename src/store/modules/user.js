import user_json from "./user.json"
import api from '../../api';

const state = {
    user: {words: {high: 1, low: 1, middle: 1}},
    user_error: ""
}

const getters = {
    USER: function (state) {
        return state.user
    },
    USER_ERROR: function (state) {
        return state.user_error
    },
    USER_SUM_WORDS: function (state) {
        var words = state.user.words
        return words.high + words.low + words.middle
    },
    USER_DATA_GRAPH: function (state) {
        var words = state.user.words
        var sum = words.high + words.low + words.middle
        var res = {}
        var one = sum / 360
        var abs_one = sum / 100
        var col_one = sum / 150

        var high = words.high / one
        var middle = words.middle / one
        var low = words.low / one

        var high_abs = words.high / abs_one
        var middle_abs = words.middle / abs_one
        var low_abs = words.low / abs_one

        var high_col = words.high / col_one
        var middle_col = words.middle / col_one
        var low_col = words.low / col_one

        res["high"] = high
        res["middle"] = middle
        res["low"] = low

        res["high_abs"] = high_abs
        res["middle_abs"] = middle_abs
        res["low_abs"] = low_abs

        res["high_col"] = high_col
        res["middle_col"] = middle_col
        res["low_col"] = low_col
        console.log("RES",res)
        return res
    },
}

const mutations = {
    "SET_USER": (state, payload) => {
        state.user = payload
    },
    "LOGIN_USER": (state, payload) => {
        console.log("LLLL_UUU", payload)
        if (!payload.errors){
            localStorage.setItem('me', JSON.stringify(payload.data.loginUser));
            state.user = payload.data.loginUser
            window.location.href = "/#/cabinet"
        }else{
            state.user_error = "Неправильное имя или пароль"
        }
    },
    "REG_USER": (state, payload) => {
        console.log(payload)
        if (!payload.errors){
            localStorage.setItem('me', JSON.stringify(payload.data.regUser));
            state.user = payload.data.regUser
            window.location.href = "/#/cabinet"
        }else{
            state.user_error = "Пользователь с такой почтой уже есть"
        }
    },
    "ADD_USER": (state, payload) => {
        localStorage.setItem('me', JSON.stringify(payload.user));
        state.user = payload.user
    },

}

const actions = {
    SET_USER: ({state, dispatch, commit}, payload) => {
        commit('SET_USER', payload)
    },
    ADD_USER: ({state, dispatch, commit}, payload) => {
        console.log("ADD_USER")
        console.log("PAYLOAD", payload)
        api.users.add(payload, 'ADD_USER', commit)
        setTimeout(function () {
            console.log("auth")
        }, 1000)
    },
    CHECK_USER: ({state, dispatch, commit}, payload) => {
        console.log("CHECK_USER")
        console.log("localStorage.ME", localStorage.me)
        if (localStorage.me) {
            api.users.getOne(JSON.parse(localStorage.me), 'ADD_USER', commit)
        } else {
            console.log("localstorage disable")
            window.location.href = "/#/sign_in"
        }
    },
    LOGIN_USER: ({state, dispatch, commit}, payload) => {
        console.log("LOGIN_USER")
        console.log("PAYLOAD", payload)
        api.users.loginUser(payload, 'LOGIN_USER', commit)
        setTimeout(function () {
            console.log("auth")
        },1000)
    },
    REG_USER: ({state, dispatch, commit}, payload) => {
        console.log("REG_USER")
        console.log("PAYLOAD", payload)
        api.users.regUser(payload, 'REG_USER', commit)
        setTimeout(function () {
            console.log("auth")
        },1000)
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
};