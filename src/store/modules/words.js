import json_words from "./words.json"
import word from "./word.json"
import api from '../../api';
import meanings from './meanings.json'
import examples from './examples.json'

const state = {
    words: [],
    word: {}
}

const getters = {
    WORDS: function (state) {
        return state.words.map(item => {
            return item
        })
    },
    WORD: function (state) {
        return state.word
    },
    EXAMPLES: function (state) {
        return state.word.examples
    },
    MEANINGS: function (state) {
        return state.word.meanings
    },
}

const mutations = {
    SET_WORDS: (state, payload) => {
        console.log("mutation set_words")
        state.words = payload
    },
    ADD_WORD: (state, payload) => {
        const val = {title: "cat", translation: "кошка"}
        state.words.push(val)
    },
    SET_WORD: (state, payload) => {
        console.log("mutation set_word")
        // state.word = word
        state.word = payload
    },
}

const actions = {
    SET_WORDS: ({state, dispatch, commit}, payload) => {
        console.log("SET_WORDS", payload)
        var limit = 10
        var offset = 0
        if (payload.limit) {
            limit = payload.limit
        }
        if (payload.offset) {
            offset = payload.offset
        }
        api.words.get(limit, offset, 'SET_WORDS', commit)
    },
    SET_WORDS_FROM_LIST: ({state, dispatch, commit}, payload) => {
        console.log("SET_WORDS_FROM_LIST")
        var user_id = JSON.parse(localStorage.getItem("me"))["id"]
        console.log("user_id", user_id)
        var req = {}
        req["user_id"] = user_id
        api.words.getWordsFromList(req, 'SET_WORDS', commit)
    },
    ADD_WORD: ({state, dispatch, commit}, payload) => {
        console.log("ADD_WORD")
        commit('ADD_WORD', payload)
    },
    SET_WORD: ({state, dispatch, commit}, payload) => {
        console.log("SET_WORD")
        api.words.getOne(payload, 'SET_WORD', commit)
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};