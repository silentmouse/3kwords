export function create(payload, mutation, func) {
    var user_id = payload["user_id"];
    var number = payload["number"];
    var kind = payload["kind"];
    console.log("pAY", payload)
    var query = `mutation createSessionWords($user_id: String!, $number: Int!,$kind: String) {
        createSessionWords(user_id: $user_id, number: $number, kind: $kind)
        {
        id 
        user_id
        words {
          id
          title
          translation
          } 
        }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {user_id, number, kind},
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('session data payload:', payload)
            func(mutation, payload.data.createSessionWords)
        });
}

export function getExercise(payload, mutation, func) {
    var session_words_id = payload;
    var query = `query getExercise($session_words_id: String!) {
       getExercise(session_words_id: $session_words_id) {
       id
       place
       word{
         id
         title
         translation
       }
       words{
         id
         title
         translation
       }
       progress
      }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {session_words_id},
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('exercise data payload:', payload)
            func(mutation, payload.data.getExercise)
        });
}

export function changeExercise(payload) {
    var id = payload["id"];
    var kind = payload["kind"];
    var query = `mutation changeExercise($id: String!, $kind: String!) {
       changeExercise(id: $id, kind: $kind){
          good
          error
       }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {id, kind},
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('exercise data payload:', payload)
            // func(mutation, payload.data.getExercise)
        });
}