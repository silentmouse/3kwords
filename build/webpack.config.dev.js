'use strict'
const webpack = require('webpack')
const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')

console.log("PROOOOOO", process.env.API_HOST)


module.exports = {
    mode: 'development',

    entry: [
        './src/app.js'
    ],
    output: {
        filename: '[name].js'
    },
    devServer: {
        hot: true,
        watchOptions: {
            poll: true
        },
        compress: true,
        port: 8081,
        allowedHosts: [
            "local.kwords.com"
        ],
    },
    resolve: {
        alias: {
            'api'	: path.join(__dirname, 'src/api/index.js'),
        },
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.styl(us)?$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'stylus-loader'
                ]
            },
            {
                test: /\.js$/,
                use: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "__API__": JSON.stringify(process.env.API_HOST)
        }),
        new webpack.HotModuleReplacementPlugin(),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: true
        })
    ]
}