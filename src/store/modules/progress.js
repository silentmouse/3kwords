import progress from "./progress.json"
import event_progress from "./event_progress.json"

const state = {
    progress: {},
    event_progress: {}
}

const getters = {
    PROGRESS: function (state) {
        return state.progress
    },
    EVENT_PROGRESS: function (state) {
        return state.event_progress
    }
}

const mutations = {
    "SET_PROGRESS": (state, payload) => {
        state.progress = progress
    },
    "SET_EVENT_PROGRESS": (state, payload) => {
        state.event_progress = event_progress
    },
    "INCREASE_PROGRESS": (state, payload) => {
        state.progress["value"] = state.progress["value"] + 10
    },
    "INCREASE_EVENT_PROGRESS": (state, payload) => {
        state.event_progress["value"] = state.event_progress["value"] + 10
    },
}

const actions = {
    SET_PROGRESS: ({state, dispatch, commit}, payload) => {
        commit('SET_PROGRESS', payload)
    },
    SET_EVENT_PROGRESS: ({state, dispatch, commit}, payload) => {
        commit('SET_EVENT_PROGRESS', payload)
    },
    INCREASE_PROGRESS: ({state, dispatch, commit}, payload) => {
        commit('INCREASE_PROGRESS', payload)
    },
    INCREASE_EVENT_PROGRESS: ({state, dispatch, commit}, payload) => {
        commit('INCREASE_EVENT_PROGRESS', payload)
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};