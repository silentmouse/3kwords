export function add(payload, mutation, func){
    var first_name = payload["first_name"];
    var last_name = payload["last_name"];
    var email = payload["email"];
    var password = payload["password"];
    var query = `mutation regUser($first_name: String!, $email: String!,$last_name: String!, $password: String!) {
        regUser(first_name: $first_name,last_name: $last_name, email: $email,password: $password)
        {id 
        last_name 
        phone 
        email
        first_name 
        name words{low high middle} 
        level
        word_count_next_level
        all_words_on_level 
        }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: { first_name, last_name, email, password },
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('user data payload:', payload)
            func(mutation,payload.data.regUser)
        });
}


export function regUser(payload, mutation, func){
    var first_name = payload["first_name"];
    var last_name = payload["last_name"];
    var email = payload["email"];
    var password = payload["password"];
    var query = `mutation regUser($first_name: String!, $email: String!,$last_name: String!, $password: String!) {
        regUser(first_name: $first_name,last_name: $last_name, email: $email,password: $password)
        {id 
        last_name 
        phone 
        email
        first_name 
        name words{low high middle} 
        level
        word_count_next_level
        all_words_on_level 
        }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: { first_name, last_name, email, password },
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('user data payload:', payload)
            func(mutation,payload)
        });
}


export function loginUser(payload, mutation, func){
    var email = payload["email"];
    var password = payload["password"];
    var query = `mutation loginUser($email: String!,$password: String!) {
        loginUser(email: $email,password: $password)
        {id 
        last_name 
        phone 
        email
        first_name 
        name words{low high middle} 
        level
        word_count_next_level
        all_words_on_level 
        }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: { email, password },
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('user data payload:', payload)
            func(mutation,payload)
        });
}



export function getOne(payload, mutation, func){
    console.log("PPPP", payload["id"])
    var id = payload["id"];
    var query = `query User($id: String!) {
        user(id: $id)
        {id 
        last_name 
        phone 
        first_name 
        name words{low high middle} 
        level
        email
        word_count_next_level
        all_words_on_level 
        }
    }`;

    fetch(__API__, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: { id },
        })
    })
        .then(r => r.json())
        .then((payload) => {
            console.log('user data returned:', payload)
            func(mutation,payload.data)
        });
}