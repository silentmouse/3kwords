module.exports = {
    words	: require("./words"),
    users	: require("./users"),
    session	: require("./session"),
};