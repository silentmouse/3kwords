import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import App from './App.vue'
import '../assets/app.styl'
import router from './router'

import store from './store'

Vue.use(VueRouter)
Vue.use(Vuetify)

new Vue({
    el: '#app',
    render: h => h(App),
    store: store,
    router: router
})