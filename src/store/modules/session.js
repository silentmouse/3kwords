import api from '../../api';

const state = {
    session_words: {},
    exercise: {word:{},words:[], progress: 0.0},
    loader: false


}

const getters = {
    SESSION_WORDS_PROGRESS: function (state) {
        return Math.round(state.exercise.progress * 100)
    },
    SESSION_WORDS: (state, payload) => {
        return state.session_words
    },
    SESSION_WORDS_WORDS: (state, payload) => {
        return state.exercise.words
    },
    EXERCISE: (state, payload) => {
        return state.exercise
    },
    WORD_EXERCISE: (state, payload) => {
        return state.exercise.word
    },
    LOADER_EX: (state, payload) => {
        return state.loader
    },

}

const mutations = {
    "CREATE_SESSION_WORDS": (state, payload) => {
        state.session_words = payload
        window.location.href = `/#/check_word/${payload.id}`
    },
    "GET_EXERCISE": (state, payload) => {
        if (payload.progress == 1){
            console.log("1000")
            window.location.href = "/#/congratulation"
        }
        state.exercise = payload
        state.loader = false
    },
}

const actions = {
    CREATE_SESSION_WORDS: ({state, dispatch, commit}, payload) => {
        var user_id = JSON.parse(localStorage.getItem("me"))["id"]
        console.log("user_id", user_id)
        var req = {}
        req["user_id"] = user_id
        req["kind"] = payload["kind"]
        req["number"] = 1
        api.session.create(req, 'CREATE_SESSION_WORDS', commit)
    },
    GET_EXERCISE: ({state, dispatch, commit}, payload) => {
        console.log("SE_ID", payload)
        state.loader = true
        api.session.getExercise(payload, 'GET_EXERCISE', commit)
    },
    CHANGE_EXERCISE: ({state, dispatch, commit}, payload) => {
        console.log("qwer")
        var req = {}
        req["id"] = payload["id"]
        if (payload["status"] == 1){
            req["kind"] = "error"
        }
        if (payload["status"] == 3){
            req["kind"] = "good"
        }
        if (payload["status"] == 2){
            req["kind"] = "complete"
        }
        api.session.changeExercise(req)
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};