import VueRouter from 'vue-router';
import Cabinet from "../components/cabinet/Cabinet.vue";
import TenWords from "../components/words/TenWords.vue";
import Repeat from "../components/repeat/Repeat.vue";
import Dictionary from "../components/words/Dictionary.vue";
import Setting from "../components/setting/Setting.vue";
import WordCard from "../components/words/WordCard.vue";
import Sentence from "../components/test/Sentence.vue";
import SignUp from "../components/registration/SignUp.vue";
import SignIn from "../components/registration/SignIn.vue";
import CheckWord from "../components/test/CheckWord.vue";
import Congratulation from "../components/Congratulation.vue";

export default new VueRouter({

    routes: [

        {

            path: '/',

            name: 'Main',

            component: Cabinet

        },

        {

            path: '/cabinet',

            name: 'Cabinet',

            component: Cabinet

        },

        {

            path: '/check_word/:session_words_id',

            name: 'CheckWord',

            component: CheckWord,

            props: true

        },

        {

            path: '/my_progress',

            name: 'MyProgress',

            component: Cabinet

        },
        {

            path: '/ten_words',

            name: 'TenWords',

            component: TenWords

        }
        ,
        {

            path: '/repeat',

            name: 'Repeat',

            component: Repeat

        }
        ,
        {

            path: '/dictionary',

            name: 'Dictionary',

            component: Dictionary

        }
        ,
        {

            path: '/dictionary/:id',

            name: 'Word',

            component: WordCard,

            props: true

        }
        ,
        {

            path: '/setting',

            name: 'Setting',

            component: Setting

        }
        ,
        {

            path: '/sentence',

            name: 'Sentence',

            component: Sentence

        }
        ,
        {

            path: '/sign_up',

            name: 'Sign Up',

            component: SignUp

        }
        ,
        {

            path: '/sign_in',

            name: 'SignIn',

            component: SignIn

        }
        ,
        {

            path: '/congratulation',

            name: 'Congratulation',

            component: Congratulation

        }

    ]

})