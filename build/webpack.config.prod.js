'use strict'
const webpack = require('webpack')
const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    mode: 'production',
    entry: [
        './src/app.js'
    ],
    output: {
        filename: '[name].js'
    },
    devServer: {
        hot: true,
        watchOptions: {
            poll: true
        },
        compress: true,
        port: 80,
        allowedHosts: [
            "local.kwords.com",
            "kwords.xyz"
        ],
    },
    resolve: {
        alias: {
            'api'	: path.join(__dirname, 'src/api/index.js'),
        },
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.styl(us)?$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'stylus-loader'
                ]
            },
            {
                test: /\.js$/,
                use: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            "__API__": JSON.stringify(process.env.API_HOST || "http://graphql.kwords.xyz/graphql")
        }),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: true
        })
    ]
}